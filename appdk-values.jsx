import { TURTLEOS as TurtleOS } from '../../turtleos';

const VALUES = {
    system: {
        flavour: TurtleOS.release.name,
        name: TurtleOS.release.prefix,
        build: TurtleOS.release.build,
        version: TurtleOS.release.version
    },
    credits: TurtleOS.credits,
    licenses: TurtleOS.license
};

export {
    VALUES
}
