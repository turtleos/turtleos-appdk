import { pm } from '../../layers/turtleos-pm/pm';

export const convertPath = (pwd, path) => {
    if (typeof path === 'number') path='/';
    const checkMy = (array, sym) => {
        let arr = array;
        if ( arr.indexOf(sym) !== -1) {
            if(sym==='..') {
                arr.splice(arr.indexOf(sym)-1,2);
            } if (sym==='~') {
                arr=['home', ...arr.slice(arr.indexOf(sym)+1)];
            }else {
                let ind = arr.indexOf(sym);
                arr.splice(ind,1);
            }
            return checkMy(arr, sym);
        } else {
            return arr;
        }
    };
    if(!path.startsWith('/')) {
    path=pwd+'/'+path;
    }
    let p = path.split("/");
    p=checkMy(p, '..');
    p=checkMy(p, '.');
    p=checkMy(p, '~');
    p.unshift('');
    let pp = (p.join('/')).replaceAll('//','/');
    return pp;
}

export const isFolder = (converted_path) => {
    const vfs = pm.getState().vfs;
    let d = vfs.readFolderSync(converted_path);
    let msg = 'No such file or directory';
    if(d.error && !d.folder) msg=`${converted_path} is not a Folder`;
    return {
        exists: d.found,
        data: d,
        msg: msg
    };
}
export const isFile = (converted_path) => {
    const vfs = pm.getState().vfs;
    let d = vfs.readFileSync(converted_path);
    let msg = 'No such file or directory';
    if(d.error && !d.file) msg=`${converted_path} as not a File but a Folder`;
    return {
        exists: d.found,
        data: d,
        msg: msg
    };
}
