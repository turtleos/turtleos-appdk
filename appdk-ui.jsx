import { pm } from '../../layers/turtleos-pm/pm';
import { json2react } from '../json2react/json2react';

import iconturtle from '../../assets/turtle-assets/turtle.png';
import iconturtleconstruction from '../../assets/turtle-assets/turtle-construction.png';
import iconturtlenoconnection from '../../assets/turtle-assets/turtle-noconnection.png';
import iconturtlesleeping from '../../assets/turtle-assets/turtle-sleeping.png';

import * as Feather from 'react-feather';
import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

const ICONS = {
    turtle: {
        simple: iconturtle,
        construction: iconturtleconstruction,
        sleeping: iconturtlesleeping,
        noconnection: iconturtlenoconnection
    },
   Feather: Feather
};
const SCREENS = {
    Construction:function (props) {return (
        <div style={{
            height:'100%',
            width:'100%',
            display:'flex',
            alignItems:'center',
            justifyContent:'center',
            flexDirection:'column'
        }}>
          <img alt="Construction" src={ICONS.turtle.construction} style={{
              minWidth:'30%',
              width:'10rem',
              maxWidth:'50%',
              maxHeight:'90%'
          }}/>
          <p>{pm.getState().lang.translate('dialog.construction')}</p>
        </div>
    );},
    NoConnection:function (props) {return (
        <div style={{
            height:'100%',
            width:'100%',
            display:'flex',
            alignItems:'center',
            justifyContent:'center',
            flexDirection:'column'
        }}>
          <img alt="No Connection" src={ICONS.turtle.noconnection} style={{
              minWidth:'30%',
              width:'10rem',
              maxWidth:'50%',
              maxHeight:'90%'
          }}/>
          <p>{pm.getState().lang.translate('dialog.noconnection')}</p>
        </div>
    );},
    Sleeping:function (props) {return (
        <div style={{
            height:'100%',
            width:'100%',
            display:'flex',
            alignItems:'center',
            justifyContent:'center',
            flexDirection:'column'
        }}>
          <img alt="Sleeping" src={ICONS.turtle.sleeping} style={{
              minWidth:'30%',
              width:'10rem',
              maxWidth:'50%',
              maxHeight:'90%'
          }}/>
          <p>{pm.getState().lang.translate('dialog.sleeping')}</p>
        </div>
    );},
};

function CustomScrollbars (conf) {
    if(conf.dref) return (
        <Scrollbars ref={conf.dref} {...conf}/>
    );
    return (
        <Scrollbars {...conf}/>
    );
};

function CustomSlider({style={}, state=false, onChange=()=>{}, knobColor='#458588', knobHoverColor='#83a598', sliderColor='#d5c4a1',}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div onClick={()=>{
            onChange(!state);
        }} style={{
            ...style,
            position:'relative',
            width:'3rem'
        }} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)}>
          <div className="base" style={{
              background:sliderColor,
              height:'0.8rem',
              width:'2.5rem',
              borderRadius:'25px',
              position:'absolute',
              top:'50%',
              left:'50%',
              transform: 'translate(-50%, -50%)',
          }}>
            <div className="knob" style={{
                height:'1.25rem',
                width:'1.25rem',
                borderRadius:'50px',
                position:'absolute',
                background:(hovered)?knobHoverColor:knobColor,
                zIndex:1,
                top:'50%',
                left:`${(state)?'75':'25'}%`,
                transition:'left .2s, background .2s',
                transform:`translate(-50%,-50%)`
            }}>
              
            </div>
          </div>
        </div>
    );
}

const GruvboxColorPalette = {
    red: ['#9d0006','#cc241d','#fb4934'],
    green:  ['#79740e','#98971a','#b8bb26'],
    yellow:  ['#b57614','#d79921','#fabd2f'],
    blue: ['#076678','#458588','#83a598'],
    purple: ['#8f3f71','#b16286','#d3869b'],
    aqua: ['#427b58','#689d6a','#8ec07c'],
    orange: ['#af3a03','#d65d0e','#fe8019'],
    gray: ['#7c6f64','#928374','#a89984'],
    black_dark: ['#1d2021','#282828','#32302f','#3c3836','#504945','#665c54','#7c6f64'],
    white_dark: ['#a89984','#bdae93','#d5c4a1','#ebdbb2','#dbf1c7'],
    white_light: ['#a89984','#bdae93','#d5c4a1','#ebdbb2','#f2e5bc','#fbf1c7','#f9f5d7'],
    black_light: ['#282828','#3c3836','#504945','#665c54','#7c6f64'],
};

function SimpleButton({palette, onClick=()=>{}, text, style={base:{},overlay:{},text:{}}, key=''}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div style={{
            borderRadius:'0.4rem',
            overflow:'hidden',
            display:'flex',
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center',
            background: palette.base,
            position:'relative',
            color:'transparent',
            padding:'0.4rem',
            margin:'0.4rem',
            fontSize:'1.1rem',
            ...style.base
        }}
             onClick={onClick}
             onMouseEnter={()=>setHovered(true)}
             onMouseLeave={()=>setHovered(false)}>
          <div style={{
              position:'absolute',
              top:0,
              left:0,
              width:'calc(100% + 0rem)',
              height:'calc(100% + 0rem)',
              color: palette.color,
              display:'flex',
              flexDirection:'row',
              alignItems:'center',
              justifyContent:'center',
              zIndex:2,
              ...style.text
          }}>
            {text}
          </div>
          {text}
          <div style={{
              width:'100%',
              height:'100%',
              clipPath:`circle(${(hovered)?'100':'0'}%)`,
              background:palette.overlay,
              transition:'clip-path .4s',
              position:'absolute',
              top:0,
              left:0,
              ...style.overlay
          }}>
            
          </div>
        </div>
    );
}

function GruvboxButton({color='blue', onClick, text, key=''}) {
    let palette = GruvboxColorPalette[color];
    return (
        <SimpleButton onClick={onClick} text={text} key={key} palette={{overlay:palette[2],base:palette[1],color:[...GruvboxColorPalette['white_dark']].pop()}}/>
    );
}

const AppdkUI = {
    // Collection of icons and icon packs
    ICONS: ICONS,

    // Some Sample Screens for all kinds of situations in life
    SCREENS: SCREENS,

    // Widgets with integrated state
    STATEFUL: {},

    // Widgets that require app to provide state
    CustomSlider:CustomSlider,

    // Widgets that don't require state
    SimpleButton: SimpleButton,
    GruvboxButton: GruvboxButton,
    Scrollbars: CustomScrollbars,

    // some libs
    json2react: json2react,
    React: React,

    // colors
    COLORS: {
        GRUVBOX: GruvboxColorPalette
    }
};
export {
    AppdkUI
}
